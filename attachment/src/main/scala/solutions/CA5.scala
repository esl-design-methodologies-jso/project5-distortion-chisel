// See LICENSE.txt for license details.
package solutions

import chisel3._

class ca5() extends Module {
    val io = IO(new Bundle {
        val in = Input(SInt(32.W))
        val out = Output(SInt(32.W))
    })

// L1
val inABS = Reg(SInt(32.W))
val inP2 = Reg(SInt(32.W))

// L2
val sign = Reg(SInt(32.W))
val inABSC10 = Reg(SInt(32.W))
val inP2C50 = Reg(SInt(32.W))
val inP3 = Reg(SInt(32.W))

// L3
val inP3C167 = Reg(SInt(32.W))

// L4
val firstSum = Reg(SInt(32.W))

// L5
val secSum = Reg(SInt(32.W))

// L1
when(io.in < 0.S) {
    inABS := -io.in
}
.otherwise {
    inABS := io.in
} 
inP2 := io.in * io.in

// L2
sign := io.in / inABS
inABSC10 := inABS * (-10.S)
inP2C50 := inP2 * (-50.S)
inP3 := inP2 * inABS

// L3
inP3C167 := inP3 * (-167.S)

// L4
firstSum := inP3C167 + inP2C50

// L5
secSum := firstSum + inABSC10
io.out := sign * secSum


}