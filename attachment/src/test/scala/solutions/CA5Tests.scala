package solutions

import chisel3.iotesters.{PeekPokeTester, Driver, ChiselFlatSpec}

class CA5Tests(c: ca5) extends PeekPokeTester(c){

  for(i <- 0 to 4){
    val x = rnd.nextInt(6)
    poke(c.io.in, x)

    for(j <- 0 to 4){
      step(1)
    }

   if(x > 0){
     expect(c.io.out , (-10*x)-(50*x*x)-(167*x*x*x))
    }
   else{
     expect(c.io.out , (-10*x)+(50*x*x)-(167*x*x*x))
    }
  }
}

class testCA5 extends ChiselFlatSpec {
 behavior of "project"
 backends foreach {backend =>
   it should s"correctly add randomly generated numbers in $backend" in {
     Driver(() => new ca5, backend)(c => new CA5Tests(c)) should be (true)
   }
 }
}
