module ca5(
  input         clock,
  input         reset,
  input  [31:0] io_in,
  output [31:0] io_out
);
  reg [31:0] inABS; // @[CA5.scala 13:16]
  reg [31:0] _RAND_0;
  reg [31:0] inP2; // @[CA5.scala 14:15]
  reg [31:0] _RAND_1;
  reg [31:0] sign; // @[CA5.scala 17:15]
  reg [31:0] _RAND_2;
  reg [31:0] inABSC10; // @[CA5.scala 18:19]
  reg [31:0] _RAND_3;
  reg [31:0] inP2C50; // @[CA5.scala 19:18]
  reg [31:0] _RAND_4;
  reg [31:0] inP3; // @[CA5.scala 20:15]
  reg [31:0] _RAND_5;
  reg [31:0] inP3C167; // @[CA5.scala 23:19]
  reg [31:0] _RAND_6;
  reg [31:0] firstSum; // @[CA5.scala 26:19]
  reg [31:0] _RAND_7;
  reg [31:0] secSum; // @[CA5.scala 29:17]
  reg [31:0] _RAND_8;
  wire  _T_19; // @[CA5.scala 32:12]
  wire [32:0] _T_21; // @[CA5.scala 33:14]
  wire [31:0] _T_22; // @[CA5.scala 33:14]
  wire [31:0] _T_23; // @[CA5.scala 33:14]
  wire [31:0] _GEN_0; // @[CA5.scala 32:19]
  wire [63:0] _T_24; // @[CA5.scala 38:15]
  wire [32:0] _T_25; // @[CA5.scala 41:15]
  wire [36:0] _T_27; // @[CA5.scala 42:19]
  wire [38:0] _T_29; // @[CA5.scala 43:17]
  wire [63:0] _T_30; // @[CA5.scala 44:14]
  wire [40:0] _T_32; // @[CA5.scala 47:18]
  wire [32:0] _T_33; // @[CA5.scala 50:22]
  wire [31:0] _T_34; // @[CA5.scala 50:22]
  wire [31:0] _T_35; // @[CA5.scala 50:22]
  wire [32:0] _T_36; // @[CA5.scala 53:20]
  wire [31:0] _T_37; // @[CA5.scala 53:20]
  wire [31:0] _T_38; // @[CA5.scala 53:20]
  wire [63:0] _T_39; // @[CA5.scala 54:16]
  wire [31:0] _GEN_1;
  wire [31:0] _GEN_2;
  wire [31:0] _GEN_3;
  wire [31:0] _GEN_4;
  wire [31:0] _GEN_5;
  wire [31:0] _GEN_6;
  wire [31:0] _GEN_7;
  assign _T_19 = $signed(io_in) < $signed(32'sh0); // @[CA5.scala 32:12]
  assign _T_21 = $signed(32'sh0) - $signed(io_in); // @[CA5.scala 33:14]
  assign _T_22 = _T_21[31:0]; // @[CA5.scala 33:14]
  assign _T_23 = $signed(_T_22); // @[CA5.scala 33:14]
  assign _GEN_0 = _T_19 ? $signed(_T_23) : $signed(io_in); // @[CA5.scala 32:19]
  assign _T_24 = $signed(io_in) * $signed(io_in); // @[CA5.scala 38:15]
  assign _T_25 = $signed(io_in) / $signed(inABS); // @[CA5.scala 41:15]
  assign _T_27 = $signed(inABS) * $signed(-32'sha); // @[CA5.scala 42:19]
  assign _T_29 = $signed(inP2) * $signed(-32'sh32); // @[CA5.scala 43:17]
  assign _T_30 = $signed(inP2) * $signed(inABS); // @[CA5.scala 44:14]
  assign _T_32 = $signed(inP3) * $signed(-32'sha7); // @[CA5.scala 47:18]
  assign _T_33 = $signed(inP3C167) + $signed(inP2C50); // @[CA5.scala 50:22]
  assign _T_34 = _T_33[31:0]; // @[CA5.scala 50:22]
  assign _T_35 = $signed(_T_34); // @[CA5.scala 50:22]
  assign _T_36 = $signed(firstSum) + $signed(inABSC10); // @[CA5.scala 53:20]
  assign _T_37 = _T_36[31:0]; // @[CA5.scala 53:20]
  assign _T_38 = $signed(_T_37); // @[CA5.scala 53:20]
  assign _T_39 = $signed(sign) * $signed(secSum); // @[CA5.scala 54:16]
  assign _GEN_1 = _T_39[31:0];
  assign io_out = $signed(_GEN_1);
  assign _GEN_2 = _T_24[31:0];
  assign _GEN_3 = _T_25[31:0];
  assign _GEN_4 = _T_27[31:0];
  assign _GEN_5 = _T_29[31:0];
  assign _GEN_6 = _T_30[31:0];
  assign _GEN_7 = _T_32[31:0];
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE
  integer initvar;
  initial begin
    `ifndef verilator
      #0.002 begin end
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{$random}};
  inABS = _RAND_0[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{$random}};
  inP2 = _RAND_1[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{$random}};
  sign = _RAND_2[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{$random}};
  inABSC10 = _RAND_3[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{$random}};
  inP2C50 = _RAND_4[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{$random}};
  inP3 = _RAND_5[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{$random}};
  inP3C167 = _RAND_6[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{$random}};
  firstSum = _RAND_7[31:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {1{$random}};
  secSum = _RAND_8[31:0];
  `endif // RANDOMIZE_REG_INIT
  end
`endif // RANDOMIZE
  always @(posedge clock) begin
    if (_T_19) begin
      inABS <= _T_23;
    end else begin
      inABS <= io_in;
    end
    inP2 <= $signed(_GEN_2);
    sign <= $signed(_GEN_3);
    inABSC10 <= $signed(_GEN_4);
    inP2C50 <= $signed(_GEN_5);
    inP3 <= $signed(_GEN_6);
    inP3C167 <= $signed(_GEN_7);
    firstSum <= _T_35;
    secSum <= _T_38;
  end
endmodule
